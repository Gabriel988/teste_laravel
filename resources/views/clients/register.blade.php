@include('elements.header')
@include('elements.sidebar')
@include('elements.navbar')

@if (session('error'))
    <div class="row bg-danger p-2 text-center">
        <div class="col-md-12">
            <p class="text-white">
                <strong>
                    {{ session('error') }}
                </strong>
            </p>
        </div>
    </div>
    <br><br>
@endif

@if (session('success'))
    <div class="row bg-success p-2 text-center">
        <div class="col-md-12">
            <h6 class="text-white">
                <strong>
                    {{ session('success') }}
                </strong>
            </h6>
        </div>
    </div>
    <br><br>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="custom-container">
            <div class="container-header container-lateral">
                <div class="header-box">
                    <i class="far fa-folder-open box-icon"></i>
                    <h4 class="box-title">Cadastrar Cliente</h4>
                </div>
            </div>
            <div class="container-body">
                <div class="row">
                    <div class="col-lg col-12 container-box">
                        <div class="custom-card">
                            <form action="{{ route('registerClient') }}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nameClient">Nome</label>
                                            <input type="text" class="form-control" id="nameClient" name="nameClient">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="type_client">Tipo</label>
                                            <select name="type_client" id="type_client" class="form-control">
                                                <option value="1"  id="physicalPerson">Pessoa Fisica</option>
                                                <option value="2">Pessoa Juridica</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="category">Categoria</label>
                                            <select name="category" id="category" class="form-control">
                                              <option value="" disabled selected>Adicione a categoria</option>
                                                @foreach ($categories as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="uf">Uf</label>
                                            <select name="uf" id="uf" class="form-control">
                                              <option value="" disabled selected>Adicione o estado</option>
                                                @foreach ($estados as $key => $value)
                                                    <option value="{{ $key }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" id="birth">
                                        <div class="form-group">
                                            <label for="birth_date">Data nascimento</label>
                                            <input type="date" class="form-control" id="birth_date" name="birth_date">
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="foundation" style="display: none;">
                                        <div class="form-group">
                                            <label for="date_foundation">Data Fundação</label>
                                            <input type="date" class="form-control" id="date_foundation"
                                                name="date_foundation">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-sm btn-success" id="add_phone">+Adicionar
                                            Telefone</button>
                                    </div>
                                </div>
                                <div class="row" id="list-phones">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="phone-1">Telefone</label>
                                            <input type="text" class="form-control" id="phone" name="phone[]">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn bnt-primary"
                                            style="float: right;">Cadastrar</button>
                                    </div>
                                </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('elements.footer')
