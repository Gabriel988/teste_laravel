@include('elements.header')
@include('elements.sidebar')
@include('elements.navbar')

<div class="row">
  <div class="col-md-12">
    <div class="custom-container">
      <div class="container-header container-lateral">
        <div class="header-box">
          <i class="fas fa-archive box-icon"></i>
          <h4 class="box-title">Categorias</h4>
        </div>
        <div class="button-box">
          <button type="button" name="button" class="button b-green"
            data-bs-toggle="modal" data-bs-target="#modal_create_category">Criar categoria</button>
        </div>
      </div>
      <div class="container-body">
        <div class="row">
          <div class="col-lg col-12 container-box">
            <div class="custom-card">
              <table class='table table-striped w-100' id="tabelaDois">
                <thead>
                  <tr>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Ação</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($categories as $row)
                    <tr>
                      <td>{{ $row->name }}</td>
                      <td>{{ $row->description }}</td>
                      <td>
                        <a href="" class="edit_category"
                          data-bs-toggle="modal" data-bs-target="#modal_edit_category"
                            data-id="{{ $row->id }}"
                            data-name="{{ $row->name }}"
                            data-description="{{ $row->description }}">
                          <i class="fa fa-search"></i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


</div>

@include('elements.footer')

<div class="modal fade" id="modal_create_category" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Adicionar Categoria</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="categorias/criar-categoria" method="post">
        @csrf
        <div class="modal-body">


          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
              	<label for="nameCategory">Nome</label>
                <input type="text" name="nameCategory" value="" class="form-control" id="nameCategory">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
              	<label for="descriptionCategory">Descrição</label>
                <textarea name="descriptionCategory" id="descriptionCategory" class="form-control" cols="30" rows="10"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Adicionar</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_edit_category" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Categoria</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="categorias/editar-categoria" method="post">
        @csrf
        @method('PUT')
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
              	<label for="edit_name">Nome</label>
                <input type="text" name="nameCategory" value="" class="form-control" id="edit_name">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
              	<label for="edit_description">Descrição</label>
                <textarea name="descriptionCategory" id="edit_description" class="form-control" cols="30" rows="10"></textarea>
              </div>
            </div>

          {{-- <div class="row">
            <div class="col-md-12">
              <div class="form-group">
              	<label for="edit_category">Categoria</label>
                <input type="text" name="edit_category" value="" class="form-control" id="edit_category">
              </div>
            </div>
          </div> --}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
          <input type="hidden" name="id_category" value="" id="id_category">
          <button type="submit" class="btn btn-primary">Editar</button>
        </div>
      </form>
    </div>
  </div>
</div>

