<?php

namespace App\Models;

use App\Models\Phones;
use App\Models\Categories;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Clients extends Model
{
  use HasFactory;

  protected $table = 'clients';
  // protected $dates = ['birth_date', 'date_foundation'];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'type_client',
    'category_id',
    'uf',
    'birth_date',
    'date_foundation'
  ];


  public function category()
  {
    return $this->hasOne(Categories::class, 'id', 'category_id');
  }

  public function phones()
  {
    return $this->hasMany(Phones::class, 'client_id', 'id');
  }
}
