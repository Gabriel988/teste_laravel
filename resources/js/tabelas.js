export default function generateTable(idTabela, idModal) {
	if (arguments.length == 2) {
		$(`#${idTabela}`).DataTable({
			"scrollX": true,
			"language": {
				"lengthMenu": "",
				"zeroRecords": "Nenhum registro encontrado",
				"info": "",
				"infoEmpty": "Nenhum registro encontrado",
				"infoFiltered": "(Filtrado de MAX registros no total)",
				"sSearch": "Buscar: ",
				"paginate": {
					"previous": "Anterior",
					"next": "Próximo",
					"first": "Primeira página",
					"last": "Última página"
				}
			},
			dropdownParent: $(`#${idModal}`)
		});
	} else if (arguments.length == 1) {
		$(`#${idTabela}`).DataTable({
			"scrollX": true,
			"language": {
				"lengthMenu": "",
				"zeroRecords": "Nenhum registro encontrado",
				"info": "",
				"infoEmpty": "Nenhum registro encontrado",
				"infoFiltered": "(Filtrado de MAX registros no total)",
				"sSearch": "Buscar: ",
				"paginate": {
					"previous": "Anterior",
					"next": "Próximo",
					"first": "Primeira página",
					"last": "Última página"
				}
			},
		});
	} else {
		return false;
	}
}
