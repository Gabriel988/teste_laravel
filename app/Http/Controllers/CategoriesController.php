<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Categories;

class CategoriesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data['categories'] = Categories::all();

    return view('category/index')->with($data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function storeCategory(Request $request)
  {
    $category = Categories::create([
      'name' => $request['nameCategory'],
      'description' => $request['descriptionCategory'],
    ]);

    return redirect('categorias');
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function updateCategory(Request $request)
  {
    $category = Categories::find($request->id_category);
    $data = [
      'name' => $request['nameCategory'],
      'description' => $request['descriptionCategory'],
    ];
    $category->update($data);
    return redirect('categorias');
  }

}
