<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper">
      <div class="navbar-toggle">
        <input type="checkbox" id="checkbox-menu">
        <label type="button" class="hamburguer-menu" for="checkbox-menu">
          <span></span>
          <span></span>
          <span></span>
        </label>
      </div>
      <p class="navbar-brand" id="custom-navbar-toggler">Teste - Laravel</p>
    </div>
  </div>
</nav>
<!-- End Navbar -->
<div class="content">
