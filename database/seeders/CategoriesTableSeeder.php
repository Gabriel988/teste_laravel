<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
  static $nameCategories = [
    'Diamante',
    'Ouro',
    'Prata',
    'Bronze',
  ];

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    foreach (self::$nameCategories as $category) {
      DB::table('categories')->insert([
        'name' => $category,
        'description' => "descrição da categoria {$category}",
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);
    }
  }
}
