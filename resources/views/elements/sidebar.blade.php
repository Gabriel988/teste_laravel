<?php $paginaCorrente = $_SERVER['REQUEST_URI'];?>
<div class="wrapper">
  <div class="custom-sidebar">
    <div class="custom-sidebar-header">
      <a href="#" class="custom-sidebar-header-link">
        Teste - Laravel
      </a>
    </div>
    <div class="custom-sidebar-body">
      <div class="custom-sidebar-body-wrapper" id="perfect-scrollbar">
        <ul class="custom-sidebar-body-wrapper-list">
          <li class="custom-sidebar-body-wrapper-list-item <?php echo(strpos($paginaCorrente, '/categorias') !== false ) ? "active" : ''; ?>">
            <a class="custom-sidebar-body-wrapper-list-item-link" href="<?php echo url('categorias');?>">
              <i class="fas fa-th-large"></i>
              <p class="custom-sidebar-body-wrapper-list-item-link-title">Categorias</p>
            </a>
          </li>
          <li class="custom-sidebar-body-wrapper-list-item <?php echo(strpos($paginaCorrente, '/clientes') !== false ) ? "active" : ''; ?>">
            <a class="custom-sidebar-body-wrapper-list-item-link" href="<?php echo url('clientes');?>">
              <i class="fas fa-users"></i>
              <p class="custom-sidebar-body-wrapper-list-item-link-title">Clientes</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="main-panel">
