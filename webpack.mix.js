const mix = require('laravel-mix');
require('laravel-mix-purgecss');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .js('resources/js/app.js', 'public/compiled/js')
  .sass('resources/sass/base.scss', 'public/compiled/css/app.css', {
    sassOptions: {
      outputStyle: 'compressed',
    },
    implementation: require('node-sass'),
  })
  // .browserSync({
  //   proxy: 'http://localhost/template-um',
  //   //* aqui se pode alterar a porta
  //   port: 3306,
  //   // port: 3306
  // })
  .version()
  // .purgeCss({
  //   enabled: true,
  // });
