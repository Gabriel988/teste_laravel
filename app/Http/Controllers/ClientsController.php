<?php

namespace App\Http\Controllers;

use App\Models\Phones;
use App\Models\Clients;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ClientsController extends Controller
{

  /**
   * Returns to the states of Brazil.
   *
   */
  private function uf()
  {
    $ufs = array(
      'AC' => 'Acre',  'AL' => 'Alagoas', 'AM' => 'Amazonas', 'AP' => 'Amapá',
      'BA' => 'Bahia',  'CE' => 'Ceará',  'DF' => 'Distrito Federal',  'ES' => 'Espirito Santo',
      'GO' => 'Goiás', 'MA' => 'Maranhão', 'MG' => 'Minas Gerais',  'MS' => 'Mato Grosso do Sul',
      'MT' => 'Mato Grosso', 'PA' => 'Pará', 'PB' => 'Paraíba',  'PE' => 'Pernambuco', 'PI' => 'Piauí',
      'PR' => 'Paraná', 'RJ' => 'Rio de Janeiro', 'RN' => 'Rio Grande do Norte',  'RO' => 'Rondônia',
      'RR' => 'Roraima', 'RS' => 'Rio Grande do Sul',  'SC' => 'Santa Catarina', 'SE' => 'Sergipe',
      'SP' => 'São Paulo',  'TO' => 'Tocantins'
    );
    return $ufs;
  }


  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data['clients'] = Clients::all();
    $data['uf'] = $this->uf();
    $data['categories'] = Categories::all();

    return view('clients/index')->with($data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $data['estados'] = $this->uf();

    $data['categories'] = Categories::all();

    return view('clients/register')->with($data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'nameClient' => 'required',
      'type_client' => 'required|max:1',
      'category' => 'required',
      'uf' => 'required',
    ]);

    if ($validator->fails()) {
      return redirect('clientes/cadastrar')->withErrors($validator)->with('error', 'Erro ao cadastrar o cliente!');
    } else {
      $client = Clients::create([
        'name' => $request->nameClient,
        'type_client' => ($request->uf == 'MG') ? '2' : $request->type_client,
        'category_id' => $request->category,
        'uf' => $request->uf,
        'birth_date' => ($request->type_client == 1) ? $request->birth_date : null,
        'date_foundation' => ($request->type_client == 2) ? $request->date_foundation : null,
      ]);

      if (!empty($request->phone[0])) {
        foreach ($request->phone as $value) {
          Phones::create([
            'client_id' => $client->id,
            'phone' => $value
          ]);
        }
      }
      
      return redirect('clientes/')->with('success', 'Cliente Cadastrado com sucesso!');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Clients  $clients
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $data['estados'] = $this->uf();
    $data['client'] = Clients::find($id);
    $data['categories'] = Categories::all();

    return view('clients/detail')->with($data);
  }

  /**
   * Filter Client.
   *
   * @param  \App\Models\Clients  $clients
   * @param  \Illuminate\Http\Request  $request
   */
  public function filterClient(Request $request)
  {
    $return = DB::table('clients as c')
      ->join('categories as cat', 'cat.id', '=', 'c.category_id')
      ->when(request('name') != '', function ($query) {
        return $query->where('c.name', 'like', '%' . request('name') . '%');
      })
      ->when(request('uf') != '', function ($query) {
        return $query->where('c.uf', request('uf'));
      })
      ->when(request('category') != '', function ($query) {
        return $query->where('c.category_id', request('category'));
      })
      ->select('c.*', 'cat.name as name_category')
      ->get();

    echo json_encode($return);
  }

  /**
   * Add new Phone.
   *
   * @param  \Illuminate\Http\Request  $request
   */
  public function addNewPhone(Request $request)
  {
    $validator = Validator::make([
      'phone' => $request->new_phone,
    ], [
      'phone' => 'required',
    ]);

    if ($validator->fails()) {
      return redirect('clientes/detalhe/' . $request->id_client)->with('error', 'Erro ao adicionar o telefone!');
    } else {
      Phones::create([
        'client_id' => $request->id_client,
        'phone' => $request->new_phone
      ]);
    }
    return redirect('clientes/detalhe/' . $request->id_client)->with('success', 'Telefone adicionado com sucesso!');
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Clients  $clients
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, int $id_client)
  {
    $validator = Validator::make([
      'nameClient' => $request->nameClient,
      'type_client' => $request->type_client,
      'category' => $request->category,
      'uf' => $request->uf,
    ], [
      'nameClient' => 'required',
      'type_client' => 'required|max:1',
      'category' => 'required',
      'uf' => 'required',
    ]);

    if ($validator->fails()) {
      return redirect('clientes/detalhe/' . $id_client)->with('error', 'Erro ao Atualizar o cliente!');
    } else {
      $client = Clients::find($id_client);
      $client->update([
        'name' => $request->nameClient,
        'type_client' => ($request->uf == 'MG') ? '2' : $request->type_client,
        'category_id' => $request->category,
        'uf' => $request->uf,
        'birth_date' => ($request->type_client == 1) ? $request->birth_date : null,
        'date_foundation' => ($request->type_client == 2) ? $request->date_foundation : null,
      ]);

      $phones = Phones::where('client_id', $id_client)->get();
      foreach ($phones as $key => $row) {
        $row->update([
          'phone' => $request->phone[$key]
        ]);
      }

      return redirect('clientes/detalhe/' . $id_client)->with('success', 'Cliente Atualizado com sucesso!');
    }
  }
}
