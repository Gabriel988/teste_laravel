<?php

use App\Http\Controllers\Index;
use Illuminate\Support\Facades\Route;

// Rotas das categorias
Route::get('categorias', 'App\Http\Controllers\CategoriesController@index')->name('inicio');
Route::post('categorias/criar-categoria', 'App\Http\Controllers\CategoriesController@storeCategory');
Route::put('categorias/editar-categoria', 'App\Http\Controllers\CategoriesController@updateCategory')->name('updateCategory');

// Rotas dos clientes
Route::get('clientes', 'App\Http\Controllers\ClientsController@index')->name('index');
Route::get('clientes/cadastrar', 'App\Http\Controllers\ClientsController@create');
Route::post('clients/registerClient', 'App\Http\Controllers\ClientsController@store')->name('registerClient');
Route::post('clients/filterClient', 'App\Http\Controllers\ClientsController@filterClient')->name('filterClient');
Route::get('clientes/detalhe/{id}', 'App\Http\Controllers\ClientsController@show');
Route::put( 'clientes/updateClient/{idClient}', 'App\Http\Controllers\ClientsController@update')->name('updateClient');
Route::post('clients/addNewPhone/', 'App\Http\Controllers\ClientsController@addNewPhone')->name('addNewPhone');
