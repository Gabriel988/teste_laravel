import generateTable from './tabelas.js';
import PerfectScrollbar from 'perfect-scrollbar';

const navbarToggler = document.getElementById('checkbox-menu');
const sidebar = document.querySelector('.custom-sidebar');
const content = document.querySelector('.main-panel');
const scrollbar = document.getElementById('perfect-scrollbar');
const baseUrl = 'http://localhost:8000/;
const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

window.onload = function () {
  (0, _tabelas_js__WEBPACK_IMPORTED_MODULE_0__.default)('tabelaUm');
  (0, _tabelas_js__WEBPACK_IMPORTED_MODULE_0__.default)('tabelaDois');
  (0, _tabelas_js__WEBPACK_IMPORTED_MODULE_0__.default)('tabelaTres'); //form masks

  $('#phone').mask('(00) 0 0000-0000');
  $('.phone').mask('(00) 0 0000-0000');
};

const filterClients = document.querySelectorAll('.filter-clients');
filterClients && Array.from(filterClients).forEach(elem => {
  elem.addEventListener('change', () => {
    const nameFilter = document.getElementById('name-filter').value;
    const ufFilter = document.getElementById('uf-filter').value;
    const categoryFilter = document.getElementById('category-filter').value;

    const formData = new FormData();
    formData.append('name', nameFilter);
    formData.append('uf', ufFilter);
    formData.append('category', categoryFilter);

    fetch(baseUrl + 'clients/filterClient', {
      method: 'post',
      headers: {
        'X-CSRF-TOKEN': csrfToken,
      },
      body: formData
    })
      .then(response => response.json())
      .then(data => {
        const tbody = document.getElementById('tbodyClients');
        tbody.innerHTML = ''
        Array.from(data).forEach(item => {
          const tr = document.createElement('tr');
          const name = document.createElement('td');
          const typeClient = document.createElement('td');
          const uf = document.createElement('td');
          const nameCategory = document.createElement('td');
          const birthDate = document.createElement('td');
          const dateFoundation = document.createElement('td');
          const tdLink = document.createElement('td');
          const a = document.createElement('a');
          name.textContent = item.name;
          typeClient.textContent = (item.type_client == '1') ? 'Fisica' : 'Jurídica';
          uf.textContent = item.uf;
          nameCategory.textContent = item.name_category;
          birthDate.textContent = item.birth_date;
          dateFoundation.textContent = item.date_foundation;
          a.href = baseUrl + 'clientes/detalhe/' + item.id;
          a.innerHTML = '<i class="fa fa-search"></i>';

          tbody.append(tr);
          tr.append(name, typeClient, uf, nameCategory, birthDate, dateFoundation, tdLink);
          tdLink.append(a);
        });
      })
      .catch(function (err) {
        console.error(err);
      })
  });
});

const type_client = document.getElementById('type_client');
type_client && type_client.addEventListener('change', () => {
  if (type_client.value == 1) {
    document.getElementById('foundation').style.display = "none";
    document.getElementById('birth').style.display = "";
  }
  if (type_client.value == 2) {
    document.getElementById('birth').style.display = "none";
    document.getElementById('foundation').style.display = "";
  }
});

const uf = document.getElementById('uf');
uf && uf.addEventListener('change', () => {
  const physicalPerson = document.getElementById('physicalPerson');
  physicalPerson.disabled = false;
  if (uf.value == 'MG') {
    type_client.value = 2;
    physicalPerson.disabled = true;
  }
});

const phone = document.getElementById('add_phone');
phone && phone.addEventListener('click', () => {
  const listPhones = document.getElementById('list-phones');
  const div = document.createElement('div');
  const div2 = document.createElement('div');
  const label = document.createElement('label');
  const input = document.createElement('input');
  div.className = 'col-md-3';
  div2.className = 'form-group';
  label.textContent = 'Telefone';
  input.className = 'form-control phone';
  input.type = 'text';
  input.id = 'phone';
  input.name = 'phone[]';

  listPhones.append(div);
  div.append(div2);
  div2.append(label, input);
  $('.phone').mask('(00) 0 0000-0000');
});

const category = document.querySelectorAll('.edit_category');
category && Array.from(category).forEach(elem => {
  elem.addEventListener('click', () => {
    let id = elem.dataset.id;
    let name = elem.dataset.name;
    let description = elem.dataset.description;

    document.getElementById('id_category').value = id;
    document.getElementById('edit_description').value = description;
    document.getElementById('edit_name').value = name;
  });
});

const scrolledSidebar = new PerfectScrollbar(scrollbar);
console.log(scrolledSidebar);

navbarToggler.addEventListener('change', () => {
  toggleNavbarState(navbarToggler, sidebar, content);
});

const toggleNavbarState = (element, target, content) => {
  if (element.checked) {
    target.classList.add('sidebar-moved');
    content.classList.add('content-moved');
  } else {
    target.classList.remove('sidebar-moved');
    content.classList.remove('content-moved');
  }
};
