@include('elements.header')
@include('elements.sidebar')
@include('elements.navbar')

 @if (session('error'))
    <div class="row bg-danger p-2 text-center">
        <div class="col-md-12">
            <p class="text-white">
                <strong>
                    {{ session('error') }}
                </strong>
            </p>
        </div>
    </div>
    <br><br>
@endif

@if (session('success'))
    <div class="row bg-success p-2 text-center">
        <div class="col-md-12">
            <h6 class="text-white">
                <strong>
                    {{ session('success') }}
                </strong>
            </h6>
        </div>
    </div>
    <br><br>
@endif

<div class="row">
  <div class="col-md-12">
    <div class="custom-container">
      <div class="container-header container-lateral">
        <div class="header-box">
          <i class="far fa-folder-open box-icon"></i>
          <h4 class="box-title">Clientes</h4>
        </div>
        <div class="button-box">
          <a class="button b-green" href="{{ url('clientes/cadastrar') }}">Cadastrar</a>
        </div>
      </div>
      <div class="container-body">
        <div class="row">
          <div class="col-lg col-12 container-box">
            <div class="custom-card">
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="">Nome</label>
                    <input type="text" class="form-control filter-clients" id="name-filter">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="">Uf</label>
                    <select name="" class="form-control filter-clients" id="uf-filter">
                      <option value=""></option>
                      @foreach ($uf as $key => $value)
                      <option value="{{ $key }}">{{ $value }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="">Categoria</label>
                    <select name="" class="form-control filter-clients" id="category-filter">
                      <option value=""></option>
                      @foreach ($categories as $key => $value)
                      <option value="{{ $value->id }}">{{ $value->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <table id="tabelaUm" class="table table-striped w-100">
                <thead class="text-primary">
                  <th>Nome</th>
                  <th>Tipo</th>
                  <th>Uf</th>
                  <th>Categoria</th>
                  <th>Data nascimento</th>
                  <th>Data Fundação</th>
                  <th>Detalhes</th>
                </thead>
                <tbody id="tbodyClients">
                  @foreach ($clients as $row)
                      <tr>
                        <td>{{ $row->name }}</td>
                        <td>{{ ($row->type_client == '1') ? 'Fisica' : 'Jurídica' }}</td>
                        <td>{{ $row->uf }}</td>
                        <td>{{ $row->category->name }}</td>
                        <td>{{ \Carbon\Carbon::parse($row->birth_date)->format('d/m/Y') }}</td>
                        <td>{{ \Carbon\Carbon::parse($row->date_foundation)->format('d/m/Y') }}</td>
                        <td>
                          <a href="{{ url('clientes/detalhe', $row->id) }}">
                            <i class="fa fa-search"></i>
                          </a>
                        </td>
                      </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('elements.footer')
