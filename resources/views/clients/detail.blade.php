@include('elements.header')
@include('elements.sidebar')
@include('elements.navbar')

@if (session('error'))
    <div class="row bg-danger p-2 text-center">
        <div class="col-md-12">
            <p class="text-white">
                <strong>
                    {{ session('error') }}
                </strong>
            </p>
        </div>
    </div>
    <br><br>
@endif

@if (session('success'))
    <div class="row bg-success p-2 text-center">
        <div class="col-md-12">
            <h6 class="text-white">
                <strong>
                    {{ session('success') }}
                </strong>
            </h6>
        </div>
    </div>
    <br><br>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="custom-container">
            <div class="container-header container-lateral">
                <div class="header-box">
                    <i class="far fa-folder-open box-icon"></i>
                    <h4 class="box-title">Detalhes do Cliente</h4>
                </div>
            </div>
            <div class="container-body">
                <div class="row">
                    <div class="col-lg col-12 container-box">
                        <div class="custom-card">
                            <form action="{{ url('clientes/updateClient/' . $client->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nameClient">Nome</label>
                                            <input type="text" class="form-control" id="nameClient" name="nameClient"
                                                value="{{ $client->name }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="type_client">Tipo</label>
                                            <select name="type_client" id="type_client" class="form-control">
                                                <option value="1" id="physicalPerson"
                                                    {{ $client->uf == 'MG' ? 'disabled' : '' }}
                                                    {{ $client->type_client == '1' ? 'selected' : '' }}>Pessoa
                                                    Fisica</option>
                                                <option value="2" {{ $client->type_client == '2' ? 'selected' : '' }}>
                                                    Pessoa
                                                    Juridica</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="category">Categoria</label>
                                            <select name="category" id="category" class="form-control">
                                                @foreach ($categories as $row)
                                                    <option value="{{ $row->id }}"
                                                        {{ $client->category_id == $row->id ? 'selected' : '' }}>
                                                        {{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="uf">Uf</label>
                                            <select name="uf" id="uf" class="form-control">
                                                @foreach ($estados as $key => $value)
                                                    <option value="{{ $key }}"
                                                        {{ $client->uf == $key ? 'selected' : '' }}>
                                                        {{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6"
                                        {{ $client->type_client == '2' ? 'style=display:none;' : '' }}>
                                        <div class="form-group">
                                            <label for="birth_date">Data nascimento</label>
                                            <input type="date" class="form-control" id="birth_date" name="birth_date"
                                                value="{{ $client->birth_date }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6"
                                        {{ $client->type_client == '1' ? 'style=display:none;' : '' }}>
                                        <div class="form-group">
                                            <label for="date_foundation">Data Fundação</label>
                                            <input type="date" class="form-control" id="date_foundation"
                                                name="date_foundation" value="{{ $client->date_foundation }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-sm btn-success"
                                          id="add_phone_modal"
                                          data-bs-toggle="modal"
                                          data-bs-target="#modalAddPhone"
                                          >+Adicionar
                                            Telefone</button>
                                    </div>
                                </div>
                                <div class="row" id="list-phones">
                                    @foreach ($client->phones as $key => $value)
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="phone-{{ $key }}">Telefone</label>
                                                <input type="text" class="form-control"
                                                    id="phone-{{ $key }}" name="phone[]"
                                                    value="{{ $value->phone }}">
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn bnt-primary"
                                            style="float: right;">Atualizar</button>
                                    </div>
                                </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('elements.footer')

<div class="modal fade" id="modalAddPhone" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Adicionar Telefone</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="{{ route('addNewPhone') }}" method="post">
        @csrf
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
              	<label for="new_phone">Telefone</label>
                <input type="text" name="new_phone" value="" class="form-control phone" id="new_phone">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
          <input type="hidden" name="id_client" value="{{ $client->id }}" id="id_client">
          <button type="submit" class="btn btn-primary">Editar</button>
        </div>
      </form>
    </div>
  </div>
</div>
